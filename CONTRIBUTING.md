# Contributors
The development of CHARM app was led and carried out by:
- Dr Anastasia Anagnostou

#### Development team
- Dr Arindam Saha supported the development of the UI. 

#### Supporting team

The following team contributed to defining the requirements:
- Dr Derek Groen
- Professor Simon J.E. Taylor
- Dr Nura Abubakar
- Dr Katie Mintram
- Ms Habiba Daroge
- Mr Tasil Islam
- Mr Maziar Ghorbani

# Acknowledgements

***This work is partially supported by the H2020 [STAMINA Project](https://cordis.europa.eu/project/id/883441)***




