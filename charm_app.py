import csv
import base64
import io, os

import dash
from dash import html, dcc
import dash_bootstrap_components as dbc

from dash import dash_table
import pandas as pd
import plotly.graph_objects as go
from dash.dependencies import Input, Output, State

from whitenoise import WhiteNoise

def Header(name, app):
    title = html.H1(name, style={"margin-top":5})
    logo = html.Img(src='./assets/logo.png', className="app_logo")
    return dbc.Row([dbc.Col(title, md=8), dbc.Col(logo, md=4)])
    
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY])
app.title = "CHARM"
server = app.server
server.wsgi_app = WhiteNoise(server.wsgi_app, root='input/')

zid = 0
zdata=[]

def create_traces(ds, obs):

    return [
        go.Scatter(
        name=obs,
        x=ds['day-mean'],
        y=ds[obs+'-mean'],
        mode='lines',
        showlegend=True,
        ),

        go.Scatter(
        name='Upper',
        x=ds['day-mean'],
        y=ds[obs+'-mean'] + ds[obs+'-ci'],
        mode='lines',
        showlegend=False,
        line=dict(width=0),
        ),

        go.Scatter(
        name='Lower',
        x=ds['day-mean'],
        y=ds[obs+'-mean'] - ds[obs+'-ci'],
        mode='lines',
        showlegend=False,
        line=dict(width=0),
        fillcolor='rgba(68, 68, 68, 0.3)',
        fill='tonexty'
        )
    ]

def plot_aggregated(df, ll):

    traces = []

    for ii in ll:
        traces.extend(create_traces(df, ii))

    figure = go.Figure(traces)

    return figure

controls_arrivals = [
        dcc.Markdown("#### Arrivals"),
        dbc.Form([
                dbc.Label("Emergency Arrivals"),
                dbc.Input(type="number", id="em_min", placeholder="min"),
                dbc.Input(type="number", id="em_max", placeholder="max"),
                dbc.Input(type="number", id="em_mode", placeholder="mode"),
                ]),
    
        dbc.Form([
                dbc.Label("Elective Arrivals"),
                dbc.Input(type="number", id="el_min", placeholder="min"),
                dbc.Input(type="number", id="el_max", placeholder="max"),
                dbc.Input(type="number", id="el_mode", placeholder="mode"),
                ]),
        ]
        
controls_los = [
        dcc.Markdown("#### Length of Stay"),
        dbc.Form([
               dbc.Label("Emergency ICU"),
               dbc.Input(type="number", id="em_los_min", placeholder="min"),
               dbc.Input(type="number", id="em_los_max", placeholder="max"),
               dbc.Input(type="number", id="em_los_mode", placeholder="mode")
               ]),
        
        dbc.Form([
               dbc.Label("Emergency recovery"),
               dbc.Input(type="number", id="emr_los_min", placeholder="min"),
               dbc.Input(type="number", id="emr_los_max", placeholder="max"),
               dbc.Input(type="number", id="emr_los_mode", placeholder="mode")
               ]),
        
        dbc.Form([
               dbc.Label("Elective ICU"),
               dbc.Input(type="number", id="el_los_min", placeholder="min"),
               dbc.Input(type="number", id="el_los_max", placeholder="max"),
               dbc.Input(type="number", id="el_los_mode", placeholder="mode")
               ]),
        
        dbc.Form([
               dbc.Label("Elective recovery"),
               dbc.Input(type="number", id="elr_los_min", placeholder="min"),
               dbc.Input(type="number", id="elr_los_max", placeholder="max"),
               dbc.Input(type="number", id="elr_los_mode", placeholder="mode")
               ]),
        
        dbc.Form([
               dbc.Label("Covid-19 ICU"),
               dbc.Input(type="number", id="c_los_min", placeholder="min"),
               dbc.Input(type="number", id="c_los_max", placeholder="max"),
               dbc.Input(type="number", id="c_los_mode", placeholder="mode")
               ]),
        
        dbc.Form([
               dbc.Label("Covid-19 recovery"),
               dbc.Input(type="number", id="cr_los_min", placeholder="min"),
               dbc.Input(type="number", id="cr_los_max", placeholder="max"),
               dbc.Input(type="number", id="cr_los_mode", placeholder="mode")
               ])
        ]       

       
controls_mortality = [
        dcc.Markdown("#### Mortality"),
        dbc.Form([
                dbc.Label("Emergency ICU mortality"),
                dbc.Input(type="number", id = "em_m", min = 0, max = 1, placeholder="0-1")
                ]),
        dbc.Form([
                dbc.Label("Emergency recovery mortality"),
                dbc.Input(type="number", id = "emr_m", min = 0, max = 1, placeholder="0-1")
                ]),
        dbc.Form([
                dbc.Label("Elective ICU mortality"),
                dbc.Input(type="number", id = "el_m", min = 0, max = 1, placeholder="0-1")
                ]),
        dbc.Form([
                dbc.Label("Elective recovery mortality"),
                dbc.Input(type="number", id = "elr_m", min = 0, max = 1, placeholder="0-1")
                ]),
        dbc.Form([
                dbc.Label("Covid-19 ICU mortality"),
                dbc.Input(type="number", id = "c_m", min = 0, max = 1, placeholder="0-1")
                ]),
        dbc.Form([
                dbc.Label("Covid-19 recovery mortality"),
                dbc.Input(type="number", id = "cr_m", min = 0, max = 1, placeholder="0-1")
                ])
        ]

controls_thresholds = [
        dcc.Markdown("#### Thresholds"),
        dbc.Form([
                dbc.Label("Upper threshold"),
                dbc.Input(type="number",id = "up", min = 0, max = 100, placeholder="0-100")
                ]),
        dbc.Form([
                dbc.Label("Lower threshold"),
                dbc.Input(type="number",id = "lo", min = 0, max = 100, placeholder="0-100")
                ])
        ]      

controls_replications = [
        dcc.Markdown("#### Replications"),
        dbc.Form([
                dbc.Label("Replications"),
                dbc.Input(type="number", id="rep", min=1, max=500, placeholder=5)
                ])
        ]    

controls_covid = [
        dcc.Markdown("#### Covid-19 arrivals"),
        dcc.Markdown("*Download an example input file*"),
        html.Button("Download CSV", id="cd-btn-csv"),
        dcc.Download(id="download"),
        html.Hr(),
        dcc.Markdown("*Upload covid-19 arrivals CSV*"),
        dcc.Upload(id="upload", children=html.Div(['Drag and Drop or ', html.A('Select file', style={'color':'orange'})]), 
                   style={
                           'lineHeight': '60px',
                           'borderWidth': '1px',
                           'borderStyle': 'dashed',
                           'borderRadus': '5px',
                           'textAlign': 'center',
                           'margin': '10px'
                           },
                   multiple=True),
        html.Div(id='upload-msg')
        ]
    
def parse_contents(contents, filename):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            df = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
            df.to_csv('./input/DAILY_ARRIVALS.csv', index=False)
        return html.Div(['File uploaded'])
    except Exception as e:
        print(e)
        return html.Div(['There was an error processing the file'])

@app.callback(Output('upload-msg', 'children'),
              Input('upload', 'contents'),
              Input('upload', 'filename')
              )

def upload(contents, filename):
    if contents is not None:
        children = [parse_contents(c, f) for c, f in
                    zip(contents, filename)]
        return children
    else:
        return html.Div([''])

controls_zones = [
        dcc.Markdown("#### Zones"),
        dbc.Form([
                dbc.Label("Zone type categories"),
                dcc.Markdown("1 - Emergency ICU"),
                dcc.Markdown("2 - Emergency recovery"),
                dcc.Markdown("3 - Elective ICU"),
                dcc.Markdown("4 - Elective recovery"),
                dcc.Markdown("5 - Covid-19 ICU"),
                dcc.Markdown("6 - Covid-19 recovery"),
                dbc.Label("Enter the number of zones"),
                dbc.Input(type="number",id = "numZones", value = 1, min = 1, max = 100, step=1)
                ]),
        html.Div(id='tab1', children=[
                        dash_table.DataTable(id='table',
                                     columns=([{'id': 'zone_id', 'name': 'Zone Id'}] +
                                     [{'id': 'zone_type', 'name': 'Zone Type'}] +
                                     [{'id': 'zone_capacity', 'name': 'Zone Capacity'}]),
                             style_as_list_view=True,
                             style_cell={'whiteSpace':'normal', 'color':'white','backgroundColor':'rgb(100,100,100)', 'textAlign':'center' ,'width':'33%'},
                             style_data={'whiteSpace':'normal', 'color':'black','backgroundColor':'white'},
                             data=[dict(zone_id=i, zone_type=1, zone_capacity=1)for i in range(1, zid)], 
                             editable=True
                             )]),
        html.Div(id='tab-msg')
        ]
        
        
@app.callback(Output('tab1', 'children'), Input('numZones', 'value')) 
def zonetable(numZones):
    global zid
    zid=int(numZones)+1
    zdata.clear()
    return [ html.Div(id='tab1', children=[
                        dash_table.DataTable(id='table',
                                     columns=([{'id': 'zone_id', 'name': 'Zone Id'}] +
                                     [{'id': 'zone_type', 'name': 'Zone Type'}] +
                                     [{'id': 'zone_capacity', 'name': 'Zone Capacity'}]),
                             style_as_list_view=True,
                             style_cell={'whiteSpace':'normal', 'color':'white','backgroundColor':'rgb(100,100,100)', 'textAlign':'center' ,'width':'33%'},
                             style_data={'whiteSpace':'normal', 'color':'black','backgroundColor':'white'},
                             data=[dict(zone_id=i, zone_type=1, zone_capacity=1)for i in range(1, zid)], 
                             editable=True
                             )])]

@app.callback(Output('tab-msg', 'children'), Input('table', 'data'))
def updatezones(data):
    global zdata
    for i in range(len(data)):
       if all(j for j in data[i].values()):
           zdata = data
         
    
@app.callback(Output('download', 'data'),
              Input('cd-btn-csv', 'n_clicks'),
              prevent_initial_call=True)
def download(n_clicks):
    return  dcc.send_file("./input/DAILY_ARRIVALS.csv")

ar_plot_list = [
          {'label': 'Emergency Arrivals', 'value': 'emArrivals'},
          {'label': 'Elective Arrivals', 'value': 'elArrivals'},
          {'label': 'Covid Arrivals', 'value': 'cArrivals'},
    ]

pat_plot_list = [
        {'label': 'ICU Patients Moved', 'value': 'icu-pat-moved'},
        {'label': 'Recovery Patients Moved', 'value': 'recovery-pat-moved'},
        {'label': 'Patients Discharged', 'value': 'pat-discharged'},
        {'label': 'Patients Died', 'value': 'pat-died'},
    ]
        
bed_plot_list = [
        {'label': 'Emergency Capacity', 'value': 'em-capacity'},
        {'label': 'Emergency Availability', 'value': 'em-available'},
        {'label': 'Elective Capacity', 'value': 'el-capacity'},
        {'label': 'Elective Availability', 'value': 'el-available'},
        {'label': 'Covid-19 Capacity', 'value': 'c-capacity'},
        {'label': 'Covid-19 Availability', 'value': 'c-available'},
        {'label': 'Emergency Recovery Capacity', 'value': 'emr-capacity'},
        {'label': 'Emergency Recovery Availability', 'value': 'emr-available'},
        {'label': 'Elective Recovery Capacity', 'value': 'elr-capacity'},
        {'label': 'Elective Recovery Availability', 'value': 'elr-available'},
        {'label': 'Covid-19 Recovery Capacity', 'value': 'cr-capacity'},
        {'label': 'Covid-19 Recovery Availability', 'value': 'cr-available'},
        ]

controls_graphs = [
             dcc.Markdown("#### Graphs"),
             html.Hr(),
             dcc.Markdown("##### Arrivals"),
             dcc.Checklist(id='ar_plot_options', options=ar_plot_list, value=[d['value']for d in ar_plot_list]),
             dbc.Button('Update arrivals graph', id='arupdate', n_clicks=0, size="sm"), 
             html.Div(id='ar_graph'),
             html.Hr(),
             dcc.Markdown("##### Patients"),
             dcc.Checklist(id='pat_plot_options', options=pat_plot_list, value=[d['value']for d in pat_plot_list]),
             dbc.Button('Update patients graph', id='patupdate', n_clicks=0, size="sm"), 
             html.Div(id='pat_graph'),
             html.Hr(),
             dcc.Markdown("##### Beds"),
             dcc.Checklist(id='bed_plot_options', options=bed_plot_list, value=[d['value']for d in bed_plot_list]),
             dbc.Button('Update beds graph', id='bedupdate', n_clicks=0, size="sm"), 
             html.Div(id='bed_graph'),
        ]

app.layout = dbc.Container(children=[
        Header("CHARM", app),
        dcc.Markdown("dynamiC Hospital wARd Management"),
        html.Hr(),
        dbc.Row([
                dbc.Col([
                        dbc.Card(controls_arrivals, body=True),
                        dbc.Card(controls_los, body=True),
                        dbc.Card(controls_mortality, body=True),
                        dbc.Card(controls_thresholds, body=True),
                        dbc.Card(controls_replications, body=True)
                        ], width=3),
        
                dbc.Col([
                        dbc.Card(controls_covid, body=True),
                        dbc.Card(controls_zones, body=True),
                        ], width=3), 
                dbc.Col([
                        dbc.Card(controls_graphs, body=True)
                        ], width=6)
            ]),
        dbc.Row([
                html.Hr(style={'margin-top': '10px'}),
                dbc.Label("Submit input parameters"),
                dbc.Button('Submit', id='submit', n_clicks=0),
                html.Hr(style={'margin-top': '10px'}),
                dbc.Label("Reset input parameters"),
                dbc.Button('Reset input', id='reset-input', n_clicks=0),
                html.Hr(style={'margin-top': '10px'}),
                dbc.Label("Run simulation"),
                dbc.Button('Run', id='run', n_clicks=0),
                ]),
        html.P(id='status', style={'margin-top': '10px', 'margin-bottom': '10px'})
        ])

@app.callback([Output('em_min', 'value'), Output('em_max', 'value'), Output('em_mode', 'value'),
         Output('el_min', 'value'), Output('el_max', 'value'), Output('el_mode', 'value'), \
         Output('em_los_min', 'value'), Output('em_los_max', 'value'), Output('em_los_mode', 'value'), \
         Output('emr_los_min', 'value'), Output('emr_los_max', 'value'), Output('emr_los_mode', 'value'), \
         Output('el_los_min', 'value'), Output('el_los_max', 'value'), Output('el_los_mode', 'value'), \
         Output('elr_los_min', 'value'), Output('elr_los_max', 'value'), Output('elr_los_mode', 'value'), \
         Output('c_los_min', 'value'), Output('c_los_max', 'value'), Output('c_los_mode', 'value'), \
         Output('cr_los_min', 'value'), Output('cr_los_max', 'value'), Output('cr_los_mode', 'value'), \
         Output('em_m', 'value'), Output('emr_m', 'value'), Output('el_m', 'value'), Output('elr_m', 'value'), \
         Output('c_m', 'value'), Output('cr_m', 'value'), Output('up', 'value'), Output('lo', 'value'), \
         Output('rep', 'value'), Output('numZones', 'value')],
         [Input('reset-input', 'n_clicks')])    
def reset(reset):
     return ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", \
             "", "", "", "", "", "", "", "", "", "", 1,]


def plot(ar_plot_options, pat_plot_options, bed_plot_options):
     df = pd.read_csv('./output/OUT_STATS.csv')
     cols = ['day-mean']
     for ii in ar_plot_options:
          cols.extend([ii+'-mean', ii+'-ci'])
     for ii in pat_plot_options:
          cols.extend([ii+'-mean', ii+'-ci'])
     for ii in bed_plot_options:
          cols.extend([ii+'-mean', ii+'-ci'])

     df = df[cols]
     
     ar_fig = plot_aggregated(df, ar_plot_options)
     ar_fig.update_xaxes(title="Days")
     ar_fig.update_yaxes(title="Number of Patients")
     pat_fig = plot_aggregated(df, pat_plot_options)
     pat_fig.update_xaxes(title="Days")
     pat_fig.update_yaxes(title="Number of Patients")
     bed_fig = plot_aggregated(df, bed_plot_options)
     bed_fig.update_xaxes(title="Days")
     bed_fig.update_yaxes(title="Number of Beds")
     plot_fig = [ar_fig, pat_fig, bed_fig]
     
     return plot_fig
    
@app.callback(
        [Output('status', 'children'), Output('ar_graph', 'children'), Output('pat_graph', 'children'), Output('bed_graph', 'children')],
        [Input('submit', 'n_clicks'), Input('run', 'n_clicks'), \
         Input('arupdate', 'n_clicks'), Input('patupdate', 'n_clicks'), Input('bedupdate', 'n_clicks'),],
        [State('em_min', 'value'), State('em_max', 'value'), State('em_mode', 'value'),
         State('el_min', 'value'), State('el_max', 'value'), State('el_mode', 'value'), \
         State('em_los_min', 'value'), State('em_los_max', 'value'), State('em_los_mode', 'value'), \
         State('emr_los_min', 'value'), State('emr_los_max', 'value'), State('emr_los_mode', 'value'), \
         State('el_los_min', 'value'), State('el_los_max', 'value'), State('el_los_mode', 'value'), \
         State('elr_los_min', 'value'), State('elr_los_max', 'value'), State('elr_los_mode', 'value'), \
         State('c_los_min', 'value'), State('c_los_max', 'value'), State('c_los_mode', 'value'), \
         State('cr_los_min', 'value'), State('cr_los_max', 'value'), State('cr_los_mode', 'value'), \
         State('em_m', 'value'), State('emr_m', 'value'), State('el_m', 'value'), State('elr_m', 'value'), \
         State('c_m', 'value'), State('cr_m', 'value'), State('up', 'value'), State('lo', 'value'), State('rep', 'value'), \
         State('ar_plot_options', 'value'), State('pat_plot_options', 'value'), State('bed_plot_options', 'value')]
)

def update(submit, run, arupdate, patupdate, bedupdate, em_min, em_max, em_mode, el_min, el_max, el_mode, em_los_min, em_los_max, em_los_mode, \
           emr_los_min, emr_los_max, emr_los_mode, el_los_min, el_los_max, el_los_mode, \
           elr_los_min, elr_los_max, elr_los_mode, c_los_min, c_los_max, c_los_mode, \
           cr_los_min, cr_los_max, cr_los_mode, em_m, emr_m, el_m, elr_m, c_m, cr_m, up, \
           lo, rep, ar_plot_options, pat_plot_options, bed_plot_options):
     ctx = dash.callback_context
     
     global zdata
     
     if submit + run == 0:
         plotf = plot(ar_plot_options, pat_plot_options, bed_plot_options)
         return ["", dcc.Graph(figure=plotf[0]), dcc.Graph(figure=plotf[1]), dcc.Graph(figure=plotf[2]),]
     elif arupdate+patupdate+bedupdate!=0:
         plotf = plot(ar_plot_options, pat_plot_options, bed_plot_options)
         return ["", dcc.Graph(figure=plotf[0]), dcc.Graph(figure=plotf[1]), dcc.Graph(figure=plotf[2]),]
     else:
         click_button_id = ctx.triggered[0]['prop_id'].split('.')[0]
         if click_button_id == 'submit':
            
              
              cols = ['em_min', 'em_max', 'em_mode', 'el_min', 'el_max', 'el_mode', 'emLoS_min', \
                    'emLoS_max', 'emLoS_mode', 'emrLoS_min', 'emrLoS_max', 'emrLoS_mode', \
                    'elLoS_min', 'elLoS_max', 'elLoS_mode', 'elrLoS_min', 'elrLoS_max', \
                    'elrLoS_mode', 'cLoS_min', 'cLoS_max', 'cLoS_mode', 'crLoS_min', 'crLoS_max', \
                    'crLoS_mode', 'em_mortality', 'emr_mortality', 'el_mortality', 'elr_mortality', \
                    'c_mortality', 'cr_mortality', 'upThreshold', 'loThreshold', 'replications']
              vals = [em_min, em_max, em_mode, el_min, el_max, el_mode, em_los_min, em_los_max, em_los_mode, \
              emr_los_min, emr_los_max, emr_los_mode, el_los_min, el_los_max, el_los_mode, \
              elr_los_min, elr_los_max, elr_los_mode, c_los_min, c_los_max, c_los_mode, \
              cr_los_min, cr_los_max, cr_los_mode, em_m, emr_m, el_m, elr_m, c_m, cr_m, up, \
              lo, rep]
              
              em = vals[0] <= vals[2] <= vals[1]
              el = vals[3] <= vals[5] <= vals[4]
              eml = vals[6] <= vals[8] <= vals[7]
              emlr = vals[9] <= vals[11] <= vals[10]
              ell = vals[12] <= vals[14] <= vals[13]
              ellr = vals[15] <= vals[17] <= vals[16]
              cl = vals[18] <= vals[20] <= vals[19]
              clr = vals[21] <= vals[23] <= vals[22]
              t =  vals[30] > vals[31] 
           
           
              if not all(i for i in vals) or not all(j for j in zdata) or not em or not el \
              or not eml or not emlr or not ell or not ellr or not cl or not clr or not t:
                  plotf = plot(ar_plot_options, pat_plot_options, bed_plot_options)
                  return ["Invalid or empty entries", dcc.Graph(figure=plotf[0]), dcc.Graph(figure=plotf[1]), dcc.Graph(figure=plotf[2]),]
              else:
                  vals = [str(x) for x in vals]
             
                  s1 = ','.join(cols)
                  s2 = ','.join(vals)

                  with open('./input/ICU_INPUT_PARAMS.csv', 'w') as ff:
                      ff.write(s1+'\n')
                      ff.write(s2+'\n')
                  
                  with open('./input/ZONES.csv', 'w') as f:
                      w = csv.DictWriter(f, zdata[0].keys())
                      w.writeheader()
                      w.writerows(zdata)
                  
                  plotf = plot(ar_plot_options, pat_plot_options, bed_plot_options)
                  return ["Input parameters submitted", dcc.Graph(figure=plotf[0]), dcc.Graph(figure=plotf[1]), dcc.Graph(figure=plotf[2]), ]
         elif click_button_id == 'run':
             os.system('python3 main.py -z ZONES.csv -p ICU_INPUT_PARAMS.csv -c DAILY_ARRIVALS.csv')
             plotf = plot(ar_plot_options, pat_plot_options, bed_plot_options)
             return ["", dcc.Graph(figure=plotf[0]), dcc.Graph(figure=plotf[1]), dcc.Graph(figure=plotf[2]),]
             
 
if __name__ == "__main__":
        app.run_server(debug=True)
